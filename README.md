# mqtt-house-subscriber

おうちのIoTを操作するためのMQTTクライアント

```shell
# usernameの設定
$ export MQTT_USERNAME=xxxxxxx

# subscribe
$ yarn sub

# publish
$ yarn pub
```
