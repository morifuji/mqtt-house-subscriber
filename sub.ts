import { connect } from "async-mqtt";

const client = connect("mqtt://mqtt.beebotte.com", {
  port: 1883,
  username: process.env.MQTT_USERNAME,
  password: ""
});

client.on("connect", ()=> {
  client.subscribe("my_house/air_off");

  client.on("message", (topic, message) => {
    console.log("topic", topic)
    console.log("message", message.toString());
  });
});
