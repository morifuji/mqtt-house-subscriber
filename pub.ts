import { connect } from "async-mqtt";

const client = connect("mqtt://mqtt.beebotte.com", {
  port: 1883,
  username: process.env.MQTT_USERNAME,
  password: "",
});

client.on("connect", async() => {
  await client.publish("my_house/ai_off", "hogehoge")
  await client.publish("my_house/ai_hot", "")
  await client.publish("my_house/ai_string", "こんにちは！")
  await client.publish("my_house/ai_string", "hello!")
  await client.publish("my_house/ai_cool", "111").then(res=>{
    console.log("###")
  })
  // process.exit(0)
});
