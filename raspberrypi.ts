import { connect } from "async-mqtt";
const { exec } = require("child_process");

const formatDate = (date: Date) => {
  return `${
    date.getMonth() + 1
  }月${date.getDate()}日 ${date.getHours()}:${date.getMinutes()}`;
};

const client = connect("mqtt://mqtt.beebotte.com", {
  port: 1883,
  username: process.env.MQTT_USERNAME,
  password: "",
});

client.on("connect", () => {
  /**
   * 疎通確認
   */
  const intervalId = setInterval(() => {
    client.publish("my_house/pi_connected", formatDate(new Date()), {
      retain: true,
      qos: 0,
    });
  }, 60 * 1000);

  /**
   * 各種subscribe
   */
  client.subscribe("my_house/air_off", {
    qos: 1,
  });
  client.subscribe("my_house/air_hot", {
    qos: 1,
  });
  client.subscribe("my_house/air_cool", {
    qos: 1,
  });

  client.on("message", handleMessage);
});

const COMMAND = (topic: string) =>
  `sudo ~/bto_ir_cmd/bto_ir_cmd -e -t  \`cat ~/bto_ir_cmd/codes/${topic.replace(
    "my_house/",
    ""
  )}\``;

const handleMessage = (topic, message) => {
  console.log("topic", topic);

  // topicをもとに発火
  exec(COMMAND(topic), (err, stdout, stderr) => {
    if (err) {
      console.log(`stderr: ${stderr}`);
      return;
    }
    console.log(`stdout: ${stdout}`);
  });
};
