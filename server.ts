import express from "express"
var path = require('path');

const app = express()
const port = 3000

app.engine('.html', require('ejs').__express);
app.set('views', path.join(__dirname, 'views'));

app.get('/', (req, res) => {
//   res.send('Hello World!')
  res.render('index', {
    users: "###",
    title: "EJS example",
    header: "Some users"
  });
})

app.set('view engine', 'html');


app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})